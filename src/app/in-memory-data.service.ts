import { InMemoryDbService } from 'angular-in-memory-web-api';

export class InMemoryDataService implements InMemoryDbService{
	
	createDb(){
		const heroes = [
			{ id: 11, name: 'Gambito' },
			{ id: 12, name: 'FireStorm' },
			{ id: 13, name: 'BlackLightning' },
			{ id: 14, name: 'Guepardo' },
			{ id: 15, name: 'Ojos de halcon' },
			{ id: 16, name: 'Flash' },
			{ id: 17, name: 'Viuda Negra' },
			{ id: 18, name: 'Flecha Verde' },
			{ id: 19, name: 'Bestia' },
			{ id: 20, name: 'PowerMan' }
		];
		return {heroes};
	}
}
